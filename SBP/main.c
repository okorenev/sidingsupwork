#include "main.h"
#include "server.h"
#include "sbp.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  int port = 3100;

  // Try to start server with provided port number
  int socket = createServer(port);
  if(socket < 0){
    printf("Error occured while trying to create server\n");
    return 1;
  }

  // Start listening to commands
  listenToServer(socket, requestHandler);

  return(0);
}

// handler for network requests from SLP
void requestHandler(char* request, char* response){

  // On connect server calles handler with NULL request
  if(request == NULL){
    //print welcome message
    sprintf(response, "Inglenook Sidings %d\n", (int)getuid());
    return;
  }

  // forward request to SBP
  commandHandler(request, response);
}
