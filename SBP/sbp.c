#include "sbp.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// private declarations
void updateStatus(char* status);
void sbpLoad(int siding1count, int siding2count, int siding3count);
void sbpTake(int siding, int wagonNum);
void sbpPut(int siding, int wagonNum);


#define WHITESPACE " \t"
#define MAX_COMMAND_PARAMS 3
#define NORMAL_POINT "NORMAL"
#define REVERSE_POINT "REVERSE"

int sidingCapacity[4] = {3, 5, 3, 3};
int sidingAmount[4] = {0, 2, 3, 3}; //default amount

int commandResult = 0;				//result of last request from SLP
bool pointOneState = true;
bool pointTwoState = true;


void commandHandler(char* command, char* status){
	int params[MAX_COMMAND_PARAMS + 1];
	int paramsCount = 0;
  char* token;

	// first get and fill all command parameters
	// split line by whitespace
	token = strtok(command, WHITESPACE);
	// check if command name exists
	if(token == NULL){
		commandResult = -4;
		updateStatus(status);
		return;
	}
	// read all params after command name and store in array
	while((token = strtok(NULL, WHITESPACE)) != NULL && paramsCount <= MAX_COMMAND_PARAMS){
		params[paramsCount] = atoi(token);
		paramsCount++;
	}

	// get command name
	token = strtok(command, WHITESPACE);

	// check params per command and call suitable command if params are ok
	if(strcmp(token, "load") == 0){
		if(paramsCount != 3){
			commandResult = -4;
		}
		else {
			sbpLoad(params[0], params[1], params[2]);
		}
	}
	else if(strcmp(token, "take") == 0){
		if(paramsCount != 2){
			commandResult = -4;
		}
		else {
			sbpTake(params[0], params[1]);
		}
	}
	else if(strcmp(token, "put") == 0){
		if(paramsCount != 2){
			commandResult = -4;
		}
		else {
			sbpPut(params[0], params[1]);
		}
	}
	else {
		commandResult = -4;
	}

	// after each request SBP returns updated status
	updateStatus(status);
	return;
}

// provides needed change to points when using particular siding
void updatePoints(int siding){
	if(siding == 1){
		pointOneState = false;
	}

	if(siding == 2){
		pointOneState = true;
		pointTwoState = false;
	}

	if(siding == 3){
		pointOneState = true;
		pointTwoState = true;
	}
}

void sbpLoad(int siding1count, int siding2count, int siding3count){

	// check if load is legal in terms of sidings capacities
	if(sidingCapacity[1] < siding1count ||
		 sidingCapacity[2] < siding2count ||
		 sidingCapacity[3] < siding3count ){

		 commandResult = -1;
		 return;
	 }

	 // update amouns
	 sidingAmount[0] = 0;
	 sidingAmount[1] = siding1count;
	 sidingAmount[2] = siding2count;
	 sidingAmount[3] = siding3count;

	 // reset points state to default
	 pointOneState = true;
	 pointTwoState = true;

	 commandResult = 0;
}

void sbpTake(int siding, int wagonCount){
	// check if siding number is legal
	if(siding <= 0 || siding > 3){
		commandResult = -3;
		return;
	}

	// check if enough space in destination siding
	if(sidingAmount[0] + wagonCount > sidingCapacity[0]){
		commandResult = -1;
		return;
	}

	// check if enough wagons in source siding
	if(sidingAmount[siding] < wagonCount){
		commandResult = -2;
		return;
	}

	sidingAmount[0] += wagonCount;
	sidingAmount[siding] -= wagonCount;
	commandResult = wagonCount;

	updatePoints(siding);

}

void sbpPut(int siding, int wagonCount){
	// check if siding number is legal
	if(siding <= 0 || siding > 3){
		commandResult = -3;
		return;
	}

	// check if enough space in destination siding
	if(sidingAmount[siding] + wagonCount > sidingCapacity[siding]){
		commandResult = -1;
		return;
	}

	// check if enough wagons in source siding
	if(sidingAmount[0] < wagonCount){
		commandResult = -2;
		return;
	}

	sidingAmount[0] -= wagonCount;
	sidingAmount[siding] += wagonCount;
	commandResult = wagonCount;

	updatePoints(siding);

}

void updateStatus(char* status){
	// updating out buffer provided by server with relevant status
	sprintf(status, "Status %d %s %s\n",
	    commandResult,
	    pointOneState ? NORMAL_POINT : REVERSE_POINT,
	    pointTwoState ? NORMAL_POINT : REVERSE_POINT);
}
