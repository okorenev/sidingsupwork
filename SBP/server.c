#include "server.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>


// creating server socket, trying to bind to port and to start listening to clients
int createServer(int server_port){
	char protoname[] = "tcp";
	struct protoent *protoent;
	int enable = 1;
	int server_sockfd;
	struct sockaddr_in server_address;

	protoent = getprotobyname(protoname);
	if (protoent == NULL) {
		return -1;
	}

	server_sockfd = socket(
		AF_INET,
		SOCK_STREAM,
		protoent->p_proto
	);

	if (server_sockfd == -1) {
		return -1;
	}

	if (setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) < 0) {
		return -1;
	}

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	server_address.sin_port = htons(server_port);

	if (bind(server_sockfd, (struct sockaddr*)&server_address, sizeof(server_address)) == -1) {
		return -1;
	}

	if (listen(server_sockfd, 5) == -1) {
		return -1;
	}

	return server_sockfd;
}

// listening to incoming connections, reading requests and immediately responsing with relevant data
void listenToServer(int server_sockfd, RequestHandler requestHandler){
	char inBuffer[BUFSIZ];
	char outBuffer[BUFSIZ];
	int client_sockfd;
	socklen_t client_len;
	ssize_t nbytes_read;
	struct sockaddr_in client_address;

	while (true) {
		client_len = sizeof(client_address);

		client_sockfd = accept(server_sockfd, (struct sockaddr*)&client_address, &client_len);

		// on connect send welcome message to client
		requestHandler(NULL, outBuffer);
		write(client_sockfd, outBuffer, strlen(outBuffer) + 1);

		while ((nbytes_read = read(client_sockfd, inBuffer, BUFSIZ)) > 0) {
			if (inBuffer[nbytes_read - 1] == '\n'){
				inBuffer[nbytes_read] = '\0';
				requestHandler(inBuffer, outBuffer);
				write(client_sockfd, outBuffer, strlen(outBuffer) + 1);
			}
		}
		
		// not closing connection (there is no such command)
		//close(client_sockfd);
	}

}
