#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


typedef void (*RequestHandler)(char*, char*);

int createServer(int port);
void listenToServer(int socket, RequestHandler requestHandler);

#endif //SERVER_H
