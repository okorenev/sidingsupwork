#include "client.h"


#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <boost/algorithm/string/predicate.hpp>


Client::Client(std::string host, int port) :
port(port), host(host) {}

// creates tcp client socket and tries to connect to server
bool Client::connect(){
	struct sockaddr_in server_address;
	struct hostent *server;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0){
		return false;
	}

	server = gethostbyname(host.c_str());
	if (server == NULL) {
			return false;
	}

	bzero((char *) &server_address, sizeof(server_address));
	server_address.sin_family = AF_INET;
	bcopy((char *)server->h_addr,
			 (char *)&server_address.sin_addr.s_addr,
			 server->h_length);
	server_address.sin_port = htons(port);

	if (::connect(sockfd,(struct sockaddr *) &server_address, sizeof(server_address)) < 0){
		return false;
	}

	return true;
}

// makes request to server and synchronously waits for response
std::string Client::request(std::string request){
	char inBuffer[BUFSIZ];


	int writeResult = write(sockfd, request.c_str(), request.length());
	if (writeResult < 0){
		std::cout << "error writing" << std::endl;
	}

	// reading only one status line from server per request
	while(true){
		int readResult = read(sockfd, inBuffer, BUFSIZ);
		if(readResult > 0 && boost::starts_with(inBuffer, "Status")){
			return inBuffer;
		}
	}
}
