#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>

class Client {

private:
	int sockfd;
	int port;
	std::string host;

public:
  Client(std::string host, int port);

	bool connect();
	std::string request(std::string request);

};


#endif //CLIENT_H
