#include <iostream>

#include "client.h"
#include "wagon.h"
#include "slp.h"

using namespace std;


int main(int argc, char *argv[]){

	// create TCP client and try to establish connection
	Client client("localhost", 3100);
	bool connectStatus = client.connect();
	if(!connectStatus){
		cout << "Unable to connect to SBP, exiting" << endl;
		return -1;
	}

	SLP slp(&client);

	// inint slp with provided files
	slp.init();
	// start slp to listen to user input
	slp.start();

	return 0;
}
