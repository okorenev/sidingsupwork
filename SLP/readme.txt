Files description:

main - trying to connect to server and initialize SLP
slp - main logic of slp
client - implementation of simple synchronous tcp client
wagon - extension of vehicle representing wagon in slp system
vehicle - base class provided as is

SLP works only with Inglenook Sidings problem, because it relies on SBP for all needed checks regarding
free space/available wagons, and SBP works only with Inglenook Sidings, so using SLP with different
wagon positions (not fiting in Inglenook Sidings problem) may cause not valid behavior
