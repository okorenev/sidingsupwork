#include "slp.h"

#include <fstream>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string.hpp>


// makes all needed initializations and prints initial state
void SLP::init(){
	loadWagonPositions();
	updateSBPWagonPositions();
	printYardState();
	drawYardState();
}

// infinetely waits for user input and executes requested commands
void SLP::start(){

	std::string line;
	// reading cin line by line
	while (getline(std::cin, line))
	{
		// spliting input by whitespace
		std::vector<std::string> dataLine;
		boost::trim_if(line, boost::is_any_of("\t "));
		boost::split(dataLine, line, boost::is_any_of("\t "), boost::token_compress_on);

		// continue if command not valid
		if(dataLine.size() != 3){
			continue;
		}

		int param1, param2, param3;

		try{
			param1 = stoi(dataLine[0]);
			param2 = stoi(dataLine[1]);
			param3 = stoi(dataLine[2]);
		}
		catch(std::invalid_argument e){
			// continue if command not valid
			continue;
		}

		executeCommand(param1, param2, param3);

		// print textual and ascii state after executing command
		printYardState();
		drawYardState();
	}

}

// reads initial wagons positions from file
void SLP::loadWagonPositions(){
	std::vector<Wagon*> wagons = loadWagonData();

	std::ifstream file("wagon_positions.txt");
	std::string str;

	while (std::getline(file, str))
	{
		// create new siding per line in file
		std::vector<Wagon*>* siding = new std::vector<Wagon*>();
		sidings->push_back(siding);

		// split line by whitespace
		std::vector<std::string> dataLine;
		boost::trim_if(str, boost::is_any_of("\t "));
		boost::split(dataLine, str, boost::is_any_of("\t "), boost::token_compress_on);

		// store capasity of current siding
		sidingCapacity.push_back(stoi(dataLine[0]));

		// read and store wagons of current siding
		for(uint i = 2; i < dataLine.size(); i++){
			Wagon* currentWagon = getWagon(wagons, std::stoi(dataLine[i]));
			if(currentWagon){
				siding->push_back(currentWagon);
			}
		}

	}

}

// reads wagon data from file and returns it
std::vector<Wagon*> SLP::loadWagonData(){
	std::ifstream file("wagon_data.txt");
	std::string str;

	std::vector<Wagon*> wagons;

	while (std::getline(file, str)){
		// tokenize line by list separator (,)
		boost::tokenizer<boost::escaped_list_separator<char> > tok(str);
		auto tokenIterator = tok.begin();

		//creating wagon instance with found tokens
		Wagon* wagon = new Wagon(tokenIterator);
		wagons.push_back(wagon);
	}

	return wagons;
}

// finds wagon in vector by serial number
Wagon* SLP::getWagon(std::vector<Wagon*> wagons, int wagonSerial){
	uint i = 0;
	Wagon* found = NULL;
	for( ; i < wagons.size(); i++){
		if(wagons[i]->getSerialNumber() == wagonSerial){
			found = wagons[i];
			break;
		}
	}

	return found;
}

// prints textual state of yard
void SLP::printYardState(){
	std::cout << "Here are sidings" << std::endl;
	for(uint i = 0; i < sidingCapacity.size() && i < sidings->size(); i++){
		std::cout << "Siding of capacity " << sidingCapacity[i] << " number of wagons " << sidings->at(i)->size() << std::endl;

		for(uint j = 0; j < sidings->at(i)->size(); j++){
			Wagon* currentWagon = sidings->at(i)->at(j);
			std::cout << *currentWagon << std::endl;
		}

		std::cout << std::endl;

	}
}

// draws yard state in ascii
void SLP::drawYardState(){

	// don't try to draw if no sidings available
	if(sidingCapacity.size() == 0){
		return;
	}

	std::vector<std::string> lines;

	// prepare string containing wagons and free space for each siding
	for(uint s = 0; s < sidingCapacity.size(); s++){
		std::stringstream line;

		std::vector<Wagon*>* currentSiding = sidings->at(s);

		for(uint p = 0; p < sidingCapacity[s]; p++){
			uint currentPlace = p;
			if(s != 0){
				// print sidings wagon in inverted order
				currentPlace = sidingCapacity[s] - p - 1;
			}
			if(currentPlace < currentSiding->size()){
				line << "=" << currentSiding->at(currentPlace)->getSerialNumber();
			}
			else {
				// free space
				line << "======";
			}
		}

		lines.push_back(line.str());
	}

	// finally draw diagram
	// first line (headshunt)
	std::cout << lines[0] << "\\" << std::endl;
	// get size of whitespace on the left part of diagram
	std::stringstream offset;
	for(uint i = 0; i < lines[0].length(); i++){
		offset << " ";
	}

	for(uint i = 1; i < lines.size(); i++){
		// add whitespace to the line start
		std::cout << offset.str();

		// add points states where needed
		if(i == 1){
			std::cout << (pointOneState ? "|" : "\\");
		}
		else if(i == 2){
			std::cout << (pointTwoState ? "|" : "\\");
		}
		else {
			// last siding doesn't have point
			std::cout << "\\";
		}

		std::cout << lines[i] << std::endl;
	}

}


// Executes command entered by user. Relies on SBP for making desicions about command correctness
void SLP::executeCommand(int sourceSiding, int destinationSiding, int wagonsNumber){
	std::stringstream command;

	// moving from headshunt to siding
	if(sourceSiding == 0){
		command << "put " << destinationSiding << " " << wagonsNumber << std::endl;
		if(executeRequest(command.str())){
			// move wagons one by one
			for(int i = 0; i < wagonsNumber; i++){
				sidings->at(destinationSiding)->push_back( sidings->at(sourceSiding)->back()	);
					sidings->at(sourceSiding)->pop_back();
			}
		}
		return;
	}

	// moving from siding to headshunt
	if(destinationSiding == 0){
		command << "take " << sourceSiding << " " << wagonsNumber << std::endl;
		if(executeRequest(command.str())){
			// move wagons one by one
			for(int i = 0; i < wagonsNumber; i++){
				sidings->at(destinationSiding)->push_back( sidings->at(sourceSiding)->back()	);
					sidings->at(sourceSiding)->pop_back();
			}

		}
		return;
	}

	// If command asks to move wagons from siding to siding (not to/from headshunt)
	// then command is splitted in two for using headshunt as buffer
	executeCommand(sourceSiding, 0, wagonsNumber);
	executeCommand(0, destinationSiding, wagonsNumber);

}

bool SLP::executeRequest(std::string request){
	std::cout << "Sent SBP: " << request;

	// sending request to TCP client
	std::string response = client->request(request);

	// splitting response by whitespace
	std::vector<std::string> dataLine;
	boost::trim_if(response, boost::is_any_of("\n\t "));
	boost::split(dataLine, response, boost::is_any_of("\n\t "), boost::token_compress_on);

	// ignore response it is not correct
	if(dataLine.size() != 4){
		return false;
	}

	// check if command succeeded
	int moved = std::stoi(dataLine[1]);
	if(moved < 0){
		std::cout << "SBP reports no wagons moved" << std::endl;
		return false;
	}

	std::cout << "SBP reports " << moved << " wagons moved" << std::endl;

	// update points states
	std::string normalPoint("NORMAL");
	pointOneState = normalPoint.compare(dataLine[2]) ? false : true;
	pointTwoState = normalPoint.compare(dataLine[3]) ? false : true;

	return true;
}

// sends load command to SBP in order to init it with correct sidings state
void SLP::updateSBPWagonPositions(){
	std::stringstream command;
	command << "load";

	for(uint i = 1; i <= 3; i++){
		if(sidings->size() > i){
			command << " " << sidings->at(i)->size();
		}
		else {
			command << " 0";
		}
	}

	command << std::endl;
	client->request(command.str());
}
