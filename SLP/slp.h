#ifndef SLP_H
#define SLP_H

#include <iostream>


#include "client.h"
#include "wagon.h"

class SLP {

private:
	Client* client;
	std::vector<uint> sidingCapacity;
	std::vector<std::vector<Wagon*>* >* sidings;

	bool pointOneState;
	bool pointTwoState;

	std::vector<Wagon*> loadWagonData();
	void loadWagonPositions();
	void updateSBPWagonPositions();
	Wagon* getWagon(std::vector<Wagon*> wagons, int wagonSerial);

	void printYardState();
	void drawYardState();
	void executeCommand(int sourceSiding, int destinationSiding, int wagonsNumber);
	bool executeRequest(std::string request);


public:
  SLP(Client* client) : client(client), pointOneState(true), pointTwoState(true) {
		sidings = new std::vector<std::vector<Wagon*>*>();
	}

	void init();
	void start();
};


#endif //SLP_H
