
#include <iostream>
#include <string>

#include "wagon.h"


std::string Wagon::streamHelper() const {
    std::stringstream os;
    os << vehicle::streamHelper() << " Carrying: " << load;
    return os.str();
}
