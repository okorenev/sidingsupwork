
#ifndef WAGON_H
#define	WAGON_H

#include <boost/tokenizer.hpp>
#include <sstream>

#include "vehicle.h"

/*!
 * \brief Class to represent a basic wagon.
 */
class Wagon : public vehicle {
public:

	/*
	 * \brief default constructor.
	 */
	Wagon() : vehicle(), load("unknown load"){}

	Wagon(const int serial_number, const std::string owner, const std::string load)
	: vehicle(serial_number, owner), load(load){}

	Wagon(boost::tokenizer<boost::escaped_list_separator<char> >::iterator &data) : vehicle(data) {
			load = *data;
	    ++data;
	}

protected:

  std::string streamHelper() const;

private:
	std::string load;
};

#endif	/* WAGON_H */
